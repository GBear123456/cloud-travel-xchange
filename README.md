# Rakuten Trqavel Xchange

:bomb::boom: [Project Link](https://solutions.travel.rakuten.com/)

Simple hotel reservation app created with Vue.js and BootstrapVue.
You can select a hotel from a list and make a reservation by choosing booking information and filling forms. Also you can make a search and filter hotels by its facilities.

Rakuten Travel Xchange is the hotel wholesale and travel technology division within the Rakuten group. We distribute our global inventory and the directly negotiated rates of Rakuten Travel and Rakuten LIFULL STAY.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/GBear123456/cloud-travel-xchange.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/GBear123456/cloud-travel-xchange/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Table of Contents

* [Features](#features)
* [Technologies](#technologies)
* [Project Setup](#project-setup)


## Features

- Rate hotels by cleanliness, comfort, staff and service,
- Add hotels to favorite
- Book the choosen hotel,
- Select check-in and check-out dates. select visitor numbers, bed size and meal,
- Set the number of forms as the number of visitors,
- Automatic move to the next form after validation,
- Opens payment modal after all forms are validated,
- Filter by facilities,
- Search by city or country name,


## Technologies

- Vue.js 2.6.11
- BootstrapVue 2.21.2
- Vuelidate 0.7.6
- Sass



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
