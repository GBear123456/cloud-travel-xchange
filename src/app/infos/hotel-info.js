// 引入 request.js
import axios from 'axios'

const COUNTRYINFO_URL='https://hiring.zumata.xyz/job01/autosuggest';
const HOTELINFO_URL='https://hiring.zumata.xyz/job01/search/';

export const getCountryInfo = () => axios.get(COUNTRYINFO_URL);
export const getHotelInfoByCity = (city) => axios.get(HOTELINFO_URL + city);