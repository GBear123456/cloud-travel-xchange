import Vue from 'vue'
import App from './App.vue'
import router from './router'

import { BootstrapVue, PaginationPlugin } from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import vueCompositionApi from '@vue/composition-api'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Custom Css
import "@/assets/scss/main.scss"

Vue.use(BootstrapVue)
Vue.use(PaginationPlugin)
Vue.use(Vuelidate)
Vue.use(vueCompositionApi)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
