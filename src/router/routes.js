export default [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home.vue')
  },
  {
    path: '/filter',
    name: 'Filter',
    component: () => import('@/views/SortFilter.vue')
  },
  {
    path: '/location',
    name: 'Location',
    component: () => import('@/views/LocationFilter.vue')
  }
]
